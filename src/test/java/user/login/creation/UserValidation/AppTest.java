package user.login.creation.UserValidation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Service
@ContextConfiguration
public class AppTest 
    extends TestCase
{
	// Create WebDriver instance
	static WebDriver driver;
	@Value("${configValue.initialPage}")
	private String initialPage;

	@Value("${configValue.url}")
	private String url;

	@Value("${configValue.validName}")
	private String validName;

	@Value("${configValue.validEmail}")
	private String validEmail;

	@Value("${configValue.validPassword}")
	private String validPassword;

	@Value("${configValue.allUserPage}")
	private String allUserPage;

	@Value("${configValue.wrongPassword}")
	private String wrongPassword;

	@Value("${configValue.invalidEmail}")
	private String invalidEmail;
	
	static Map < String, Object[] > testReport = new TreeMap < String, Object[] >();
	
	//Create blank workbook
    static XSSFWorkbook workbook = new XSSFWorkbook(); 
    //Create a blank sheet
    static XSSFSheet spreadsheet = workbook.createSheet(" Employee Info ");
  //Create row object
    static XSSFRow row;
    
    public static String deleteApiStatus = "";

	@Before
	public void setUp() throws Exception {
		// Initialize the WebDriver instance using chrome and launch the web browser
		System.setProperty("webdriver.chrome.driver", "./Chrome/chromedriver.exe");
		driver = new ChromeDriver();
		// Open the application 
		driver.navigate().to(url);
		// Maximize the current window
		driver.manage().window().maximize();
	}
	@BeforeClass
	public static void init() throws Exception {
		testReport.put( "0", new Object[] {"TEST CASE", "STATUS", "NOTE" });
		deleteApi();
	}
	
	
	@Test
	public void checkInitialPage() throws Exception {
		
		WebElement selectedItem  = driver.findElement(By.className("page-header"));
		if(!selectedItem.getText().equalsIgnoreCase(initialPage)){
			testReport.put( "2", new Object[] {"Is automation script launch the valid site", "Fail", "" });
			assertTrue("Automation script did not launch valid site, please check.", false);
		}else{
			testReport.put( "3", new Object[] {"Is automation script launch the valid site", "Pass", "" });
		}
		
		
	}

	@Test
	public void checkCreatedUserValue() throws Exception {
		boolean nameFlag = false; 
		boolean emailFlag = false;
		WebElement selectedItem  = driver.findElement(By.id("name"));
		selectedItem .sendKeys(validName);
		selectedItem  = driver.findElement(By.id("email"));
		selectedItem .sendKeys(validEmail);
		selectedItem  = driver.findElement(By.id("password"));
		selectedItem .sendKeys(validPassword);
		selectedItem  = driver.findElement(By.id("confirmationPassword"));
		selectedItem .sendKeys(validPassword);
		driver.findElement(By.xpath("//*[text() = 'Submit']")).click();

		selectedItem  = driver.findElement(By.className("page-header"));
		if(!selectedItem.getText().equalsIgnoreCase(allUserPage)){
			testReport.put( "4", new Object[] {"After submit user get transfer to valid page.", "Fail", "" });
			assertTrue("After submit user not get transfer to valid page.", false);
		}else{
			testReport.put( "5", new Object[] {"After submit user get transfer to valid page.", "Pass", "" });
			List<WebElement> TRCollection = driver.findElement(By.id("users")).findElements(By.tagName("tr"));

			for (WebElement tr : TRCollection) 
			{
				List<WebElement> TDCollection = tr.findElements(By.tagName("td"));
				for (WebElement td: TDCollection) 
				{
					if(td.getText().equalsIgnoreCase(validName)){
						nameFlag = true;
					}
					if(td.getText().equalsIgnoreCase(validEmail)){
						emailFlag = true;
					}
				}
			}
			if(nameFlag && emailFlag){
				testReport.put( "6", new Object[] {"Newly created user value is present into the list", "Pass", "Password field should be encrypted" });
			}else{
				testReport.put( "7", new Object[] {"Newly created user value is present into the list", "Fail", "Password field should be encrypted" });
				assertTrue("Newly created user value is not present into the list.", false);
			}
		}
		
	}

	@Test
	public void checkAllUserFuctionality() throws Exception {

		driver.findElement(By.xpath("//*[text() = 'All User']")).click();
		WebElement selectedItem  = driver.findElement(By.className("page-header"));
		if(!selectedItem.getText().equalsIgnoreCase(allUserPage)){
			testReport.put( "8", new Object[] {"After clicking on all user, is user get transfer to valid page", "Fail", "" });
			assertTrue("After clicking on all user, user not get transfer to valid paga.", false);
		}else{
			testReport.put( "9", new Object[] {"After clicking on all user, is user get transfer to valid paga", "Pass", "" });
		}
	}

	@Test
	public void passwordLengthValidationLe() throws Exception {

		WebElement selectedItem  = driver.findElement(By.id("name"));
		selectedItem .sendKeys(validName+"New");
		selectedItem  = driver.findElement(By.id("email"));
		selectedItem .sendKeys(validEmail+"New");
		selectedItem  = driver.findElement(By.id("password"));
		selectedItem .sendKeys(wrongPassword);
		selectedItem  = driver.findElement(By.id("confirmationPassword"));
		selectedItem .sendKeys(wrongPassword);
		driver.findElement(By.xpath("//*[text() = 'Submit']")).click();

		selectedItem  = driver.findElement(By.className("page-header"));
		if(selectedItem.getText().equalsIgnoreCase(allUserPage)){
			testReport.put( "10", new Object[] {"Password should be 6 digits long", "Fail", "Ex. "+wrongPassword });
			assertTrue("Password should be 6 digits long.", false);
		}else{
			testReport.put( "10", new Object[] {"Password should be 6 digits long", "Pass", "" });
		}
	}

	@Test
	public void allFieldsValidation() throws Exception {

		driver.findElement(By.xpath("//*[text() = 'Submit']")).click();
		WebElement nameFlag = driver.findElement(By.id("user.name.error"));
		WebElement emailFlag = driver.findElement(By.id("user.email.error"));
		WebElement passwordFlag = driver.findElement(By.id("user.password.error"));
		WebElement confirmationPasswordFlag = driver.findElement(By.id("user.confirmationPassword.error"));

		if(nameFlag.getText().equalsIgnoreCase("Required")){
			if(emailFlag.getText().equalsIgnoreCase("Required")){
				if(passwordFlag.getText().equalsIgnoreCase("Required")){
					if(confirmationPasswordFlag.getText().equalsIgnoreCase("Required")){
						testReport.put( "11", new Object[] {"All fields validation is present on New User page", "Pass", "" });
					}else{
						testReport.put( "15", new Object[] {"All fields validation is present on New User page", "Fail", "Confirmation password field should be mandatory" });
						assertTrue("password is required for user creation.", false);
					}
				}else{
					testReport.put( "14", new Object[] {"All fields validation is present on New User page", "Fail", "Password field should be mandatory" });
					assertTrue("password is required for user creation.", false);
				}
			}else{
				testReport.put( "13", new Object[] {"All fields validation is present on New User page", "Fail", "Email field should be mandatory" });
				assertTrue("email is required for user creation.", false);
			}
		}else{
			testReport.put( "12", new Object[] {"All fields validation is present on New User page", "Fail", "Name field should be mandatory " });
			assertTrue("name is required for user creation.", false);
		}
	}

	@Test
	public void checkUniqueName() throws Exception {

		WebElement selectedItem  = driver.findElement(By.id("name"));
		selectedItem .sendKeys(validName.toUpperCase());
		selectedItem  = driver.findElement(By.id("email"));
		selectedItem .sendKeys(validEmail+"NewTest");
		selectedItem  = driver.findElement(By.id("password"));
		selectedItem .sendKeys(validPassword);
		selectedItem  = driver.findElement(By.id("confirmationPassword"));
		selectedItem .sendKeys(validPassword);
		driver.findElement(By.xpath("//*[text() = 'Submit']")).click();

		selectedItem  = driver.findElement(By.className("page-header"));
		if(selectedItem.getText().equalsIgnoreCase(allUserPage)){
			testReport.put( "16", new Object[] {"Name should be case insensitive and unique", "Fail", "Allowed to enter same name with uppercase and lowercase changes " });
			assertTrue("Name should be case insensitive and unique.", false);
		}else{
			testReport.put( "17", new Object[] {"Name should be case insensitive and unique", "Pass", "Did not allowed to enter same name with uppercase and lowercase changes" });
		}
	}

	@Test
	public void checkEmailValidation() throws Exception {

		boolean flag = false ;
		String[] email = invalidEmail.split(",");
		WebElement selectedItem  = driver.findElement(By.id("name"));
		for(int i=0 ; i<email.length ; i++){
			selectedItem  = driver.findElement(By.id("email"));
			selectedItem .sendKeys(email[i]);
			driver.findElement(By.xpath("//*[text() = 'Submit']")).click();
			WebElement emailFlag = driver.findElement(By.id("user.email.error"));
			if(emailFlag.getText().equalsIgnoreCase("Invalid email address")){
				flag = true;
			}else{
				testReport.put( "18", new Object[] {"Check email Validation ", "Fail", "Ex. "+email[i] });
				assertTrue("email Validation failed for "+email[i]+" mail id", false);
			}
		}
			if(flag){
				testReport.put( "17", new Object[] {"Check email Validation ", "Pass", "Ex. "+invalidEmail });
			}
	}

	@Test
	public void checkUniqueEmail() throws Exception {

		WebElement selectedItem  = driver.findElement(By.id("name"));
		selectedItem .sendKeys(validName+"checkUniqueEmail");
		selectedItem  = driver.findElement(By.id("email"));
		selectedItem .sendKeys(validEmail.toUpperCase());
		selectedItem  = driver.findElement(By.id("password"));
		selectedItem .sendKeys(validPassword);
		selectedItem  = driver.findElement(By.id("confirmationPassword"));
		selectedItem .sendKeys(validPassword);
		driver.findElement(By.xpath("//*[text() = 'Submit']")).click();

		selectedItem  = driver.findElement(By.className("page-header"));
		if(selectedItem.getText().equalsIgnoreCase(allUserPage)){
			testReport.put( "19", new Object[] {"Emil should be case insensitive and unique", "Fail", "Allowed to enter same email with uppercase and lowercase changes"});
			assertTrue("Email should be case insensitive and unique.", false);
		}else{
			testReport.put( "20", new Object[] {"Email should be case insensitive and unique", "Pass", "Did not allowed to enter same email with uppercase and lowercase changes"});
		}
	}

	@Test
	public void confirmationPasswordValidation() throws Exception {

		WebElement selectedItem  = driver.findElement(By.id("name"));
		selectedItem .sendKeys(validName+"NewPassTest");
		selectedItem  = driver.findElement(By.id("email"));
		selectedItem .sendKeys(validEmail+"NewPassTest");
		selectedItem  = driver.findElement(By.id("password"));
		selectedItem .sendKeys(validPassword+"NewPassTest");
		selectedItem  = driver.findElement(By.id("confirmationPassword"));
		selectedItem .sendKeys(validPassword+"NewTestCase");
		driver.findElement(By.xpath("//*[text() = 'Submit']")).click();

		WebElement emailFlag = driver.findElement(By.id("user.confirmationPassword.error"));
		if(emailFlag.getText().equalsIgnoreCase("passwords are not the same")){
			testReport.put( "21", new Object[] {"Password and confirm password field should contain same value", "Pass", ""});
		}else{
			testReport.put( "22", new Object[] {"Password and confirm password field should contain same value", "Fail", ""});
			assertTrue("confirmation password validation failed", false);
		}
	}

	@Test
	public void passwordEncriptionValidation() throws Exception {

		boolean passwordFlag = false;
		driver.findElement(By.xpath("//*[text() = 'All User']")).click();
		List<WebElement> TRCollection = driver.findElement(By.id("users")).findElements(By.tagName("tr"));

		for (WebElement tr : TRCollection) 
		{
			int count = 1; 
			List<WebElement> TDCollection = tr.findElements(By.tagName("td"));
			for (WebElement td: TDCollection) 
			{
				if(count == 3){
					if(td.getText().equalsIgnoreCase(validPassword)){
						testReport.put( "22", new Object[] {"User password should be encrypted", "Fail", "Password is visible"});
						assertTrue("User password should be encrypted.", false);
					}else{
						passwordFlag = true;
					}
				}
				count++;
			}
		}
		if(passwordFlag){
			testReport.put( "23", new Object[] {"User password should be encrypted", "Pass", "Password is encrypted"});
		}
	}

	@Test
	public void passwordSpaceValidation() throws Exception {

		WebElement selectedItem  = driver.findElement(By.id("name"));
		selectedItem .sendKeys(validName+"SpaceValidation");
		selectedItem  = driver.findElement(By.id("email"));
		selectedItem .sendKeys(validEmail+"SpaceValidation");
		selectedItem  = driver.findElement(By.id("password"));
		selectedItem .sendKeys(validPassword+"   SpaceValidation");
		selectedItem  = driver.findElement(By.id("confirmationPassword"));
		selectedItem .sendKeys(validPassword+"   SpaceValidation");
		driver.findElement(By.xpath("//*[text() = 'Submit']")).click();

		selectedItem  = driver.findElement(By.className("page-header"));
		if(selectedItem.getText().equalsIgnoreCase(allUserPage)){
			testReport.put( "24", new Object[] {"Password should not contain space", "Fail", "Ex. "+validPassword+"   SpaceValidation"});
			assertTrue("Password should not contain space.", false);
		}else{
			testReport.put( "25", new Object[] {"Password should not contain space", "Pass", "Ex. "+validPassword+"   SpaceValidation"});
		}
	}

	@Test
	public void newUserFuctionality() throws Exception {

		driver.findElement(By.xpath("//*[text() = 'All User']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[text() = 'New User']")).click();
		Thread.sleep(5000);
		WebElement selectedItem  = driver.findElement(By.className("page-header"));

		if(!selectedItem.getText().equalsIgnoreCase(initialPage)){
			testReport.put( "25", new Object[] {"After clicking on new user, user get transfer to valid paga", "Fail", ""});
			assertTrue("After clicking on new user, user not get transfer to valid paga.", false);
		}else{
			testReport.put( "26", new Object[] {"After clicking on new user, user get transfer to valid paga", "Pass", ""});
		}
	}

	@Test
	public void refreshValidation() throws Exception {

		WebElement selectedItem  = driver.findElement(By.id("name"));
		selectedItem .sendKeys(validName+"SpaceValidation");
		selectedItem  = driver.findElement(By.id("email"));
		selectedItem .sendKeys("email");
		selectedItem  = driver.findElement(By.id("password"));
		selectedItem .sendKeys(validPassword+"   SpaceValidation");
		selectedItem  = driver.findElement(By.id("confirmationPassword"));
		selectedItem .sendKeys(validPassword+"   SpaceValidation");
		driver.findElement(By.xpath("//*[text() = 'Submit']")).click();

		String nameValue=driver.findElement(By.id("name")).getAttribute("value");
		String emailValue=driver.findElement(By.id("email")).getAttribute("value");
		if(nameValue == null || nameValue.equalsIgnoreCase("")){
			testReport.put( "26", new Object[] {"If validation error occurred after clicking on submit then, fields should hold the current values", "Fail", ""});
			assertTrue("If validation error occurred after clicking on submit then, fields should hold the current values.", false);
		}else{
			if(emailValue == null || emailValue.equalsIgnoreCase("")){
				testReport.put( "27", new Object[] {"If validation error occurred after clicking on submit then, fields should hold the current values", "Fail", ""});
				assertTrue("If validation error occurred after clicking on submit then, fields should hold the current values.", false);
			}else{
				testReport.put( "28", new Object[] {"If validation error occurred after clicking on submit then, fields should hold the current values", "Pass", ""});
			}
		}

	}

	@Test
	public void nameSpaceValidation() throws Exception {
		String name = "";
		for(int i = 0 ; i<40 ; i++){
			name = name+"   ";
		}
		name = name + "nameSpaceValidation";
		for(int i = 0 ; i<40 ; i++){
			name = name+"   ";
		}
		WebElement selectedItem  = driver.findElement(By.id("name"));
		selectedItem .sendKeys(name);
		selectedItem  = driver.findElement(By.id("email"));
		selectedItem .sendKeys(validEmail+"nameSpaceValidation");
		selectedItem  = driver.findElement(By.id("password"));
		selectedItem .sendKeys(validPassword+"nameSpaceValidation");
		selectedItem  = driver.findElement(By.id("confirmationPassword"));
		selectedItem .sendKeys(validPassword+"nameSpaceValidation");
		driver.findElement(By.xpath("//*[text() = 'Submit']")).click();
			if(driver.getPageSource().contains(allUserPage)){
				selectedItem  = driver.findElement(By.className("page-header"));
				if(selectedItem.getText().equalsIgnoreCase(allUserPage)){
					testReport.put( "29", new Object[] {"Name field should not contain spaces at starting and ending places ", "Fail", ""});
					assertTrue("Name field should not contain spaces at starting and ending places .", false);
				}else{
					testReport.put( "30", new Object[] {"Name field should not contain spaces at starting and ending places ", "Pass", ""});
				}
			}else{
				testReport.put( "31", new Object[] {"Name field should not contain spaces at starting and ending places ", "Fail", "Code error occurred if we enter big name field containing whitespace. See the error screenshot named as nameSpaceValidation.png in screenshot folder from project folder structure. PersistenceException occured : org.hibernate.exception.DataException: could not execute statement, In /app/controllers/Users.java (around line 47)"});
				File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				try {
					// now copy the  screenshot to desired location using copyFile //method
					FileUtils.copyFile(src, new File("./screenshot/nameSpaceValidation.png"));
				}
				catch (IOException io){
					System.out.println(io.getMessage());
				}
				assertTrue("Name field should not contain spaces at starting and ending places. Code error occurred if we enter big name field containing whitespace. See the error screenshot named as nameSpaceValidation.png in screenshot folder from project folder structure. PersistenceException occured : org.hibernate.exception.DataException: could not execute statement, In /app/controllers/Users.java (around line 47)", false);
			}
			
	}

	@After
	public void tearDown() {
		// Quit the launched web browser
		driver.quit();
	}
	

	@Configuration
	@ComponentScan("user.login.creation.UserValidation")
	static class someConfig {

		// because @PropertySource doesnt work in annotation only land
		@Bean
		PropertyPlaceholderConfigurer propConfig() {
			PropertyPlaceholderConfigurer ppc =  new PropertyPlaceholderConfigurer();
			ppc.setLocation(new ClassPathResource("configValue.properties"));
			return ppc;
		}
	}
	
	@AfterClass
	public static void createReport(){
	      
	    //Iterate over data and write to sheet
	      Set < String > keyid = testReport.keySet();
	      int rowid = 0;
	      for (String key : keyid)
	      {
	         row = spreadsheet.createRow(rowid++);
	         Object [] objectArr = testReport.get(key);
	         int cellid = 0;
	         for (Object obj : objectArr)
	         {
	            Cell cell = row.createCell(cellid++);
	            cell.setCellValue((String)obj);
	         }
	      }
	      //Write the workbook in file system
	      FileOutputStream out;
		try {
			out = new FileOutputStream(new File("./automatedTestReport/ReportSheet.xlsx"));
			workbook.write(out);
		    out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	      System.out.println("\n");
	      System.out.println("----------------------------------------------------------------------------------------------------------------------");
	      System.out.println("ReportSheet.xlsx created successfully.");
	      System.out.println( "----------------------------------------------------------------------------------------------------------------------");
	      System.out.println("\n");
	      
	      System.out.println("----------------------------------------------------------------------------------------------------------------------");
	      System.out.println(deleteApiStatus+".");
	      System.out.println( "----------------------------------------------------------------------------------------------------------------------");
	      System.out.println("\n");
	      
	      System.out.println("----------------------------------------------------------------------------------------------------------------------");
	      System.out.println("Maven build completed. You can find detailed test case report in automatedTestReport folder present in current project structure.");
	      System.out.println( "----------------------------------------------------------------------------------------------------------------------");
	      System.out.println("\n");
	      
	      System.out.println("----------------------------------------------------------------------------------------------------------------------");
	      System.out.println("You can also find the error screenshot occurred during the execution in screenshot folder of current project structure (if any).Test case report contain detailed discription about occurred error.");
	      System.out.println( "-----------------------------------------------------------------------------------------------------------------------");
	      System.out.println("\n");
	      
	}
	public static void deleteApi(){
		ApiCall apiCall = new ApiCall();
		try{
		String result = apiCall.deleteUserApi();
		if(result.equalsIgnoreCase("HTTP/1.1 200 OK")){
				testReport.put( "1", new Object[] {"Delete Api is working fine", "Pass", result });
				throw new Exception("Delete Api is working");
		   }else{
			   	testReport.put( "1", new Object[] {"Delete Api is working", "Fail", "Delete Api is not working. It may affect the test case results, please make sure all data get deleted "+ result});
			   	throw new Exception("Delete Api is not working. It may affect the test case results, please make sure all data get deleted");
		   }
		}catch(Exception e){
			deleteApiStatus = e.getMessage();
		}
	}
}
