package user.login.creation.UserValidation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ValueBean {

	@Value("${configValue.initialPage}")
	private String initialPage;

	@Value("${configValue.url}")
	private String url;

	@Value("${configValue.validName}")
	private String validName;

	@Value("${configValue.validEmail}")
	private String validEmail;

	@Value("${configValue.validPassword}")
	private String validPassword;

	@Value("${configValue.allUserPage}")
	private String allUserPage;

	@Value("${configValue.wrongPassword}")
	private String wrongPassword;

	@Value("${configValue.invalidEmail}")
	private String invalidEmail;

	/**
	 * @return the initialPage
	 */
	public String getInitialPage() {
		return initialPage;
	}

	/**
	 * @param initialPage the initialPage to set
	 */
	public void setInitialPage(String initialPage) {
		this.initialPage = initialPage;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the validName
	 */
	public String getValidName() {
		return validName;
	}

	/**
	 * @param validName the validName to set
	 */
	public void setValidName(String validName) {
		this.validName = validName;
	}

	/**
	 * @return the validEmail
	 */
	public String getValidEmail() {
		return validEmail;
	}

	/**
	 * @param validEmail the validEmail to set
	 */
	public void setValidEmail(String validEmail) {
		this.validEmail = validEmail;
	}

	/**
	 * @return the validPassword
	 */
	public String getValidPassword() {
		return validPassword;
	}

	/**
	 * @param validPassword the validPassword to set
	 */
	public void setValidPassword(String validPassword) {
		this.validPassword = validPassword;
	}

	/**
	 * @return the allUserPage
	 */
	public String getAllUserPage() {
		return allUserPage;
	}

	/**
	 * @param allUserPage the allUserPage to set
	 */
	public void setAllUserPage(String allUserPage) {
		this.allUserPage = allUserPage;
	}

	/**
	 * @return the wrongPassword
	 */
	public String getWrongPassword() {
		return wrongPassword;
	}

	/**
	 * @param wrongPassword the wrongPassword to set
	 */
	public void setWrongPassword(String wrongPassword) {
		this.wrongPassword = wrongPassword;
	}

}
