package user.login.creation.UserValidation;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;


public class ApiCall {
	
	public String deleteUserApi() throws ClientProtocolException, IOException {
	   // Given
	   HttpUriRequest request = new HttpDelete( "http://85.93.17.135:9000/user/all" );
	 
	   // When
	   HttpResponse response = HttpClientBuilder.create().build().execute( request );
	 
	   // Then
	   return response.getStatusLine().toString();
	}
}
