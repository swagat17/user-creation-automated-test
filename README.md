# Introduction
Automated test case for new user creating scenario. 

# Build 
```shell
mvn clean package install -DskipTests
```
# Note
- `mvn clean package install -DskipTests` command will download the required dependencies but will not execute  the test case.
- Run `mvn -Dtest=AppTest test` command. This will start executing the test cases.
- After completion of test case it will provide you result and the location of generated report sheet.
- You can find detailed test case report in `automatedTestReport` folder present in current project structure.
- You can also find the error screenshot occurred during the execution in `screenshot` folder of current project structure. Test case report contains detailed discription about occurred error (if any).

# Running API Server
#### - Prerequisite
- Java 8
- Maven 3
- Junit 
- Selenium
- Git

#### - Available Webservice Actions
```shell
GET /user/all/json Get all Users
```
```shell
DELETE /user/all Delete all users
```
# Running Test Case Manually
#### - Using IDE
- Open compatible IDE find the file named as AppTest.java or go to `\src\test\java` find the folder location `\user\login\creation\UserValidation` and open the `AppTest.java` file
- Right click on that file and run as Junit test.
- This will execute the file and generate report sheet and take a screenshot of error (if any).
- All these folders will get available in parallel to `POM.xml`
- For deatiled logs you can refer console and Junit tab from IDE